/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cisco.eloquapoc.models;

/**
 *
 * @author amperiya
 */
import java.util.List;

public class Contact 
{
	public String accountName;
	public String address1;
	public String address2;
	public String address3;
	public String businessPhone;
	public String city;
	public String country;
	public String firstName;
	public String emailAddress;
	public int id;	
	public boolean isBounceback;
	public boolean isSubscribed;
	public String lastName;
	public String salesPerson;
	public String title;
	public List fieldValues;
}