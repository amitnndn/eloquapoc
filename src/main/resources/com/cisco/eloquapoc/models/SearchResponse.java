/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cisco.eloquapoc.models;

/**
 *
 * @author amperiya
 */
import java.util.List;

public class SearchResponse<T> 
{
	public List<T> elements;
	public int total;
	public int page;
	public int pageSize;
}
