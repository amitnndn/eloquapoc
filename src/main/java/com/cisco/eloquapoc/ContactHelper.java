/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cisco.eloquapoc;

import com.cisco.eloquapoc.model.Contact;
import com.cisco.eloquapoc.model.ContactElement;
import com.cisco.eloquapoc.model.ContactResponse;
import com.cisco.eloquapoc.model.SyncModal;
import com.cisco.eloquapoc.model.SyncResponse;
import com.cisco.rest.api.*;
import com.google.gson.Gson;
import java.util.ArrayList;
import org.json.simple.JSONObject;

/**
 *
 * @author amperiya
 */
public class ContactHelper {
    private Client _client;
	
    public ContactHelper(String site, String user, String password, String baseUrl) 
    {
            _client = new Client(site + "\\" + user, password, baseUrl);
    }
    
    public Response PostContact(ArrayList<Contact> data,String eMail){
        Gson gson = new Gson();
        try{
            Response response = _client.post("/api/bulk/2.0/contacts/imports/88/data?format=json", gson.toJson(data));
            
            SyncModal syncModal = new SyncModal();
            syncModal.syncedInstanceUri = "/contacts/imports/88";
            Response syncResponse = _client.post("/api/bulk/2.0/syncs?format=json",gson.toJson(syncModal));
            System.out.println(syncResponse.body);
            SyncResponse synRep = gson.fromJson(syncResponse.body,SyncResponse.class);
            String uri = "/api/bulk/2.0" + synRep.uri + "?format=json";
            try {
                Thread.sleep(20000);                 //1000 milliseconds is one second.
                EmailHelper emailHelper = new EmailHelper();
                Response searchContactResponse = _client.get("/api/rest/2.0/data/contact/view/100007/contacts?search=common_fields='"+eMail+"'");
                System.out.println(searchContactResponse.body);
                ContactResponse contactResponse = gson.fromJson(searchContactResponse.body,ContactResponse.class);
                ContactElement contactElement = contactResponse.elements[0];
                String contactId = contactElement.contactId;
                String emailData = emailHelper.buildEmailData(contactId,"72","4");
                Response emailResponse = _client.post("/api/rest/1.0/assets/email/deployment?format=json", emailData);
                System.out.println(emailResponse.body);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            Response finalResponse = _client.get(uri);
            System.out.println(finalResponse.body);
             
            return response;
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
