/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cisco.eloquapoc;

import com.cisco.eloquapoc.model.Email;
import com.cisco.eloquapoc.model.EmailTestDeployment;
import com.google.gson.Gson;

/**
 *
 * @author amperiya
 */
public class EmailHelper {
    
    public String buildEmailData(String contactId, String emailId, String emailGroupId) {
        try{
            Gson gson = new Gson();
            EmailTestDeployment deployment = new EmailTestDeployment();
            
            Email email = new Email();
            email.name = "sampleemail1";
            email.id = Integer.parseInt(emailId);
            email.emailGroupId = Integer.parseInt(emailGroupId);
            
            deployment.contactId = contactId;
            deployment.email = email;
            deployment.name = "Email Deployment";
            deployment.type = "EmailTestDeployment";
            
            String requestBody = gson.toJson(deployment);
            return requestBody;
        }
        catch(Exception e){
            e.printStackTrace();
            return "false";
        }
    }
}
