/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cisco.eloquapoc.model;

import com.cisco.eloquapoc.model.RawHtmlContent;

/**
 *
 * @author amperiya
 */
public class Email {
    public int emailGroupId; 
    public RawHtmlContent htmlContent;
    public int id; 
    public boolean isPlainTextEditable; 
    public String name; 
    public String plainText; 
    public boolean sendPlainTextOnly; 
    public String subject; 
}
