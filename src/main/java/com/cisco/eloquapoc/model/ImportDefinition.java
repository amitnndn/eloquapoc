package com.cisco.eloquapoc.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Class responsible for the Import Definition data structure
 * @author Serge Borysov
 *
 */
public class ImportDefinition {
	public String id;
	public String parentId;
	public String name;
	public Map <String, String> fields = new HashMap<String,String>();
	public String identifierFieldName;
	public String isSyncTriggeredOnImport;
	public String uri;
	//experimental
	public String updateRule = "always";
}
