/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cisco.eloquapoc.model;

/**
 *
 * @author amperiya
 */
public class SyncResponse {
    public String syncedInstanceUri;
    public String syncStartedAt;
    public String status;
    public String createdAt;
    public String createdBy;
    public String uri;
}
