package com.cisco.eloquapoc.controller;

import com.cisco.eloquapoc.ContactHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cisco.eloquapoc.model.Contact;
import com.cisco.eloquapoc.model.ImportDefinition;
import com.google.gson.Gson;
import com.cisco.rest.api.*;
import java.util.ArrayList;
import org.json.simple.JSONObject;

@Controller
@RequestMapping("/sendMail")
public class SendMail {

	@RequestMapping(value = {"{eMail}/{firstName}/{lastName}/{phone}"}, method = RequestMethod.GET)
        
	public @ResponseBody
	Response SendContact(@PathVariable("eMail") String eMail,@PathVariable("firstName") String firstName, @PathVariable("lastName") String lastName,@PathVariable("phone") String phone) {
                JSONObject jsonObj = new JSONObject();
                JSONObject contactObj = new JSONObject();
                Gson gson = new Gson();
                Response response = new Response();
                ArrayList<Contact> contacts = new ArrayList<Contact>();
                Contact contact = new Contact();
                contact.C_EmailAddress = eMail;
                contact.C_FirstName = firstName;
                contact.C_LastName = lastName;
                contacts.add(contact);
                
                ContactHelper contactHelper = new ContactHelper("CMCWebExSandBox","Amit.Periyapatna","Nandan1!","https://secure.p04.eloqua.com");
                response = contactHelper.PostContact(contacts,eMail);
                
                
		return response;
	}
        public ImportDefinition getContactPersonPartyImportDefintion(){
                ImportDefinition importDef = new ImportDefinition();
                importDef.name = "Import Test";
                importDef.fields.put("C_FirstName","{{Contact.Field(C_FirstName)}}");
                importDef.fields.put("C_LastName","{{Contact.Field(C_LastName)}}");
                importDef.fields.put("C_EmailAddress","{{Contact.Field(C_EmailAddress)}}");
                importDef.identifierFieldName = "C_EmailAddress";
                importDef.isSyncTriggeredOnImport = "false";
                return importDef;
        }

}