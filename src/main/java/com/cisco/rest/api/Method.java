package com.cisco.rest.api;

public enum Method 
{
	GET,
	POST,
	PUT,
	DELETE
}
