<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
<link rel="stylesheet" href="../css/bootstrap.css">
<script src="../js/bootstrap.js"></script>
<style>
    #thankYou{
        display: none;
        font-weight: bold;
        font-size: 25px;
        text-align: center;
    }
    #signUpHeader{
        margin-left: 300px;
        text-align: left;
    }
    #submit{
        margin-left: -100px;
    }
</style>
<title> Free Trial Sign Up.</title>	
</head>
<body background="https://www.webex.com/content/dam/webex/eopi/Americas/USA/en_us/global/images/hero/slid5.jpg" style="background-repeat:repeat-x; width: 780px;">
<div class="container">
  <h2 align="center" id="signUpHeader">Free Trial Signup</h2>
  <form class="form-horizontal" role="form" onsubmit="Ajaxrequest()" id="formSubmit">
  <div class="form-group">
      <label class="control-label col-sm-2" >First Name:</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" id="fn">
      </div>
    </div>
	<div class="form-group">
      <label class="control-label col-sm-2" >Last Name:</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" id="ln" />
      </div>
    </div>
	<div class="form-group">
      <label class="control-label col-sm-2">Company:</label>
      <div class="col-sm-5">
        <input type="text" class="form-control" id="company" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-5">
        <input type="email" class="form-control" id="email" />
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-5">          
        <input type="password" class="form-control" id="pwd" />
      </div>
    </div>
	<div class="form-group">        
      <div class="col-sm-offset-5 col-sm-10">
          <input type="submit" class="btn btn-default" id="submit"/>
      </div>
    </div>
	</form>
  <div id="thankYou">Thank you for Registering for a free trial</div>
    <script>
        $(document).ready(function(){
            $("#submit").on("click",function(e){
                e.preventDefault();
                var email = $("#email").val();
                var firstName = $("#fn").val();
                var lastName = $("#ln").val();
                var url = "/eloquaPoc/rest/sendMail/"+email+"/"+firstName+"/"+lastName+"/123456789/";
                $("#thankYou").show();
                $("#formSubmit").hide();
                $("#signUpHeader").hide();
                console.log("URL : " + url);
                $.get(url, function(data){
                    console.log("Success");
                })
            })
        })
    </script>
<body>
</html>